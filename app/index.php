<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 09.07.18
 * Time: 13:51
 */


$containers = explode("\n", shell_exec("docker ps -a --format '{{.Names}}'"));

$containers = array_filter($containers, function ($item) {
  return !empty($item);
});

$hosts = file_get_contents('/var/app/hosts');

list($custom_hosts, $docker_hosts) = explode("##docker", $hosts);
$docker_hosts = "";

foreach ($containers as $container) {
  $config = shell_exec("docker container inspect $container --format='{{json .Config}}'");
  $network = shell_exec("docker container inspect $container --format='{{json .NetworkSettings}}'");

  $config = json_decode($config);
  $network = json_decode($network);
  $network = (array) $network->Networks;
  $network = array_shift($network);

  if (!empty($network->IPAddress)) {
    $docker_hosts .= PHP_EOL . "{$network->IPAddress}    {$config->Hostname}    {$config->Domainname}" . PHP_EOL;
  }
}

file_put_contents("/var/app/hosts", implode("##docker", [
  $custom_hosts, $docker_hosts
]));