FROM php

RUN apt-get update
RUN apt-get install lsb-release apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-key fingerprint 0EBFCD88
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
RUN apt-get update
RUN apt-get install docker-ce -y


RUN mkdir -p /var/app

#ADD ./app /var/app

WORKDIR /var/app

RUN apt-get install net-tools nano wget mc -y